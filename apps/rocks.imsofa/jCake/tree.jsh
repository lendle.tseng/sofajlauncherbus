//DEPS org.jgrapht:jgrapht-core:1.5.1
import org.jgrapht.*;
import org.jgrapht.graph.*;
import org.jgrapht.graph.builder.*;
import java.util.*;
import org.jgrapht.alg.tour.*;

Graph<String, DefaultEdge> g=
GraphTypeBuilder.<String, DefaultEdge> undirected().edgeClass(DefaultEdge.class).buildGraph();
g.addVertex("a");
g.addVertex("b");
g.addVertex("c");
g.addVertex("d");
g.addVertex("e");
g.addVertex("f");
g.addVertex("g");
g.addEdge("a", "b");
g.addEdge("b", "c");
g.addEdge("c", "f");
g.addEdge("c", "g");
g.addEdge("c", "d");
g.addEdge("d", "e");

while(g.vertexSet().size()>2){
    List<String> toBeDeleted=new ArrayList<>();
    for(String v : g.vertexSet()){
        if(g.degreeOf(v)==1){
            toBeDeleted.add(v);
        }
    }
    for(String v: toBeDeleted){
        g.removeVertex(v);
    }
}

System.out.println(g.vertexSet());