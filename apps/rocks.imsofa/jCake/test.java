class Main{
    public static long factorial(long n){
        // System.out.println("factorial("+n+")");
        if(n==0){
            return 1;
        }else{
            return n*factorial(n-1);
        }
    }

    public static void main(String [] args) throws Exception{
        System.out.println(factorial(12000));
    }
}