//DEPS org.openjfx:javafx-controls:17.0.2:${os.detected.jfxname}
//DEPS org.openjfx:javafx-graphics:17.0.2:${os.detected.jfxname}
//DEPS org.openjfx:javafx-fxml:17.0.2:${os.detected.jfxname}
//DEPS org.openjfx:javafx-web:17.0.2:${os.detected.jfxname}

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import yzu.imsofa.network.javapracticetool.*;

import java.io.IOException;
import javafx.event.EventHandler;
import javafx.scene.image.Image;
import javafx.stage.WindowEvent;
import com.github.mouse0w0.darculafx.DarculaFX;

/**
 * JavaFX App
 */
public class launcher extends Application {

    private static Scene scene;

    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("primary.fxml"));
        Parent parent=fxmlLoader.load();
        scene = new Scene(parent, 1200, 768);
        DarculaFX.applyDarculaStyle(scene);
        final PrimaryController controller=fxmlLoader.getController();
        //stage.getIcons().add(new Image(launcher.class.getResourceAsStream("jcake.png")));
        stage.setOnCloseRequest(new EventHandler<WindowEvent>(){
            @Override
            public void handle(WindowEvent event) {
                controller.terminateCurrentProcess();
                System.exit(0);
            }
        });
        stage.setScene(scene);
        stage.setTitle("jCake");
        stage.show();
    }

    static void setRoot(String fxml) throws IOException {
        scene.setRoot(loadFXML(fxml));
    }

    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(launcher.class.getResource(fxml + ".fxml"));
        return fxmlLoader.load();
    }

    public static void main(String[] args) {
        launch();
    }

}
