//DEPS com.github.freva:ascii-table:1.8.0
import com.github.freva.asciitable.*;
import java.util.*;

boolean [][] cases=new boolean[][]{
    {false, false},
    {false, true},
    {true, false},
    {true, true}
};


String [] headers=new String[]{"p", "q", "!(p && q)", "!p || !q"};
List<String[]> rows=new ArrayList<>();
for(int i=0; i<cases.length; i++){
    String [] row=new String[]{
        ""+cases[i][0], ""+cases[i][1], 
        ""+!(cases[i][0] && cases[i][1]),
        ""+(!cases[i][0] || !cases[i][1])
    };
    rows.add(row);
}

String [][] data=rows.toArray(new String[0][0]);

System.out.println(AsciiTable.getTable(headers, data));