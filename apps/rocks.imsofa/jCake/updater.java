//DEPS com.github.kevinsawicki:http-request:5.3
//DEPS com.google.code.gson:gson:2.10.1
//DEPS commons-io:commons-io:2.11.0
//DEPS org.openjfx:javafx-controls:17.0.2:${os.detected.jfxname}
//DEPS org.openjfx:javafx-graphics:17.0.2:${os.detected.jfxname}
//DEPS org.openjfx:javafx-fxml:17.0.2:${os.detected.jfxname}
//DEPS org.openjfx:javafx-web:17.0.2:${os.detected.jfxname}

import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.event.EventHandler;
import javafx.scene.image.Image;
import javafx.stage.WindowEvent;
import javafx.scene.control.*;
import java.io.*;
import java.util.*;
import com.github.kevinsawicki.http.*;
import org.apache.commons.io.*;
import com.google.gson.*;
import javafx.scene.layout.*;
import javafx.application.*;

public class updater extends Application {

    private static Scene scene;
	String localVersion="-1";
    @Override
    public void start(Stage stage) throws IOException {
		BorderPane parent=new BorderPane();
		final ProgressBar pb=new ProgressBar(0);
		parent.setBottom(pb);
		pb.setPrefWidth(500);
		final Label message=new Label("checking version......");
		parent.setCenter(message);
        scene = new Scene(parent, 500, 300);
		
        stage.setScene(scene);
        stage.setTitle("jCake Updater");
		
		File versionFile = new File("version.json");
		Gson gson=new Gson();
		
		if(versionFile.exists()){
			Map versionInfo=gson.fromJson(FileUtils.readFileToString(versionFile, "utf-8"), Map.class);
			Map latestInfo=(Map)versionInfo.get("latest");
			localVersion=""+latestInfo.get("version");
		}
					
		Thread updaterThread=new Thread(()->{
			try{
				Thread.sleep(1000);
				HttpRequest checkVersionRequest=HttpRequest.get("https://www.dropbox.com/s/5nubs4spdu8yzyj/version.json?dl=1").followRedirects(true);
				if(checkVersionRequest.ok()){
					File newVersionFile = new File("version.new.json");
					checkVersionRequest.receive(newVersionFile);
					Map newVersionInfo=gson.fromJson(FileUtils.readFileToString(newVersionFile, "utf-8"), Map.class);
					Map newLatestInfo=(Map)newVersionInfo.get("latest");
					String newVersion=""+newLatestInfo.get("version");
					
					if(localVersion.equals(newVersion)==false){
						Platform.runLater(()->{
							message.setText(message.getText()+"\r\nversion "+newVersion+" found, downloading......");
						});
						FileUtils.copyFile(newVersionFile, versionFile);
						FileUtils.deleteQuietly(newVersionFile);
					}else{
						System.exit(0);
					}
					
					String src=""+newLatestInfo.get("src");
					double total=(double)newLatestInfo.get("size");
					HttpRequest request =  HttpRequest.get(src).followRedirects(true);
					if (request.ok()) {
						final File file = new File("JavaPracticeTool-1.0-SNAPSHOT-jar-with-dependencies.jar.new");
						Thread t=new Thread(()->{
							double percent=0;
							while(percent<1){
								percent=file.length()/total;
								pb.setProgress(percent);
								try{
									Thread.sleep(500);
								}catch(Exception e){}
							}
							
						});
						t.setDaemon(true);
						t.start();
						request.receive(file);
						pb.setProgress(1);
						Platform.runLater(()->{
							message.setText(message.getText()+"\r\nupdate completed, re-launch to apply");
						});
						Thread.sleep(3000);
					}
				}
				System.exit(0);
			}catch(Throwable e){e.printStackTrace();}
		});
		stage.setOnCloseRequest(new EventHandler<WindowEvent>(){
            @Override
            public void handle(WindowEvent event) {
                System.exit(0);
            }
        });
		updaterThread.start();
		stage.setAlwaysOnTop(true);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }

}
/*class Updater{
	public static void main(String [] args) throws Exception{
		HttpRequest checkVersionRequest=HttpRequest.get("https://www.dropbox.com/s/5nubs4spdu8yzyj/version.json?dl=1").followRedirects(true);
		if(checkVersionRequest.ok()){
			File versionFile = new File("version.json");
			checkVersionRequest.receive(versionFile);
			Gson gson=new Gson();
			Map versionInfo=gson.fromJson(FileUtils.readFileToString(versionFile, "utf-8"), Map.class);
			Map latestInfo=(Map)versionInfo.get("latest");
			String src=""+latestInfo.get("src");
			double total=(double)latestInfo.get("size");
			HttpRequest request =  HttpRequest.get(src).followRedirects(true);
			if (request.ok()) {
				final File file = new File("JavaPracticeTool-1.0-SNAPSHOT-jar-with-dependencies.jar.new");
				Thread t=new Thread(()->{
					double percent=0;
					while(percent<1){
						percent=file.length()/total;
						System.out.println((percent*100)+"% downloaded");
						try{
							Thread.sleep(500);
						}catch(Exception e){}
					}
					
				});
				t.setDaemon(true);
				t.start();
				request.receive(file);
				System.out.println((100)+"% downloaded");
			}
		}
	}
}*/
