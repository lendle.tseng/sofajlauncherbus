//DEPS org.jgrapht:jgrapht-core:1.5.2
import org.jgrapht.*;
import org.jgrapht.graph.*;
import org.jgrapht.graph.builder.*;
import java.util.*;
import org.jgrapht.alg.connectivity.*;
import org.jgrapht.alg.cycle.*;
import org.jgrapht.traverse.*;

Graph<String, DefaultEdge> g=GraphTypeBuilder.<String, DefaultEdge> undirected().
    edgeClass(DefaultEdge.class).buildGraph();

g.addVertex("a");
g.addVertex("b");
g.addVertex("c");
g.addVertex("d");
g.addVertex("e");

g.addEdge("a", "b");
g.addEdge("c", "a");
g.addEdge("c", "d");
g.addEdge("b", "d");
g.addEdge("d", "e");

// DepthFirstIterator<String, DefaultEdge> it=new DepthFirstIterator<>(g, "a");
// while(it.hasNext()){
//     String v=it.next();
//     System.out.println(v);
// }

Stack<String> stack=new Stack();
Map<String, Boolean> visitedMap=new HashMap<>();
for(String v : g.vertexSet()){
    visitedMap.put(v, false);
}
stack.push("a");
visitedMap.put("a", true);
// System.out.println("a");


// Set<DefaultEdge> e2=g.outgoingEdgesOf("a");
// System.out.println(e2.size());

while(!stack.isEmpty()){
    String v=stack.pop();
    // System.out.println("v="+v);
    Set<DefaultEdge> edges=g.outgoingEdgesOf(v);
    for(DefaultEdge e : edges){
        String target=g.getEdgeTarget(e);
        String source=g.getEdgeSource(e);
        if(target.equals(v)){
            target=source;
        }
        // System.out.println("target="+target+": source=" +source);
        if(visitedMap.get(target)==false){
            System.out.println(target);
            visitedMap.put(target, true);
            stack.push(target);
        }
    }
}

System.out.println(visitedMap);