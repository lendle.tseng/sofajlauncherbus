class TreeNode{
    private double value=-1;
    private TreeNode leftChild=null;
    private TreeNode rightChild=null;

    public TreeNode(double v){
        value=v;
    }

    public double getValue(){
        return value;
    }

    public TreeNode getLeftChild(){
        return this.leftChild;
    }

    public void setLeftChild(TreeNode node){
        this.leftChild=node;
    }

    public TreeNode getRightChild(){
        return this.rightChild;
    }

    public void setRightChild(TreeNode node){
        this.rightChild=node;
    }

    public TreeNode binary_search(double value){
        return binary_search(this, value);
    }

    private TreeNode binary_search(TreeNode node, double value){
        if(node==null){
            return null;
        }
        if(node.getValue()==value){
            return node;
        }
        else if(node.getValue()<value){
            return binary_search(node.getRightChild(), value);
        }else{
            return binary_search(node.getLeftChild(), value);
        }
    }

    public TreeNode add(double value){
        return add(this, value);
    }

    private TreeNode add(TreeNode node, double value){
        if(node.getValue()==value){
            return node;
        }
        else if(node.getValue()<value){
            if(node.getRightChild()==null){
                TreeNode newNode=new TreeNode(value);
                node.setRightChild(newNode);
                return newNode;
            }else{
                return add(node.getRightChild(), value);
            }
        }else{
            if(node.getLeftChild()==null){
                TreeNode newNode=new TreeNode(value);
                node.setLeftChild(newNode);
                return newNode;
            }else{
                return add(node.getLeftChild(), value);
            }
        }
    }
}

TreeNode root=new TreeNode(50);
TreeNode node40=new TreeNode(40);
TreeNode node70=new TreeNode(70);
TreeNode node30=new TreeNode(30);
TreeNode node45=new TreeNode(45);
TreeNode node60=new TreeNode(60);
root.setLeftChild(node40);
root.setRightChild(node70);
node40.setLeftChild(node30);
node40.setRightChild(node45);
node70.setLeftChild(node60);

TreeNode node=root.binary_search(30);
System.out.println(root.add(80));
