import java.util.*;

class test1710229981252 {
	public static void main(String[] args) throws Exception {

		SimpleSet a = SimpleSet.createSet(5, 1, 3);
		SimpleSet b = SimpleSet.createSet(6, 3);
		boolean ret = a.isSuperSetOf(b);
		System.out.println("isSuperSetOf=" + ret);
		SimpleSet c = a.or(b);
		System.out.println(c);
	}
}

class SimpleSet {
		private LinkedList list = new LinkedList();
		private static int count = 0;

		public void add(Object o) {
			if (this.contains(o)) {
				return;
			}
			list.add(o);
		}

		public void remove(Object o) {
			this.list.remove(o);
		}

		public boolean contains(Object o) {
			return list.contains(o);
		}

		public Object[] toArray() {
			return list.toArray();
		}

		public String toString() {
			Object[] array = this.toArray();
			return Arrays.toString(array);
		}

		public boolean isSuperSetOf(SimpleSet s) {
			Object[] array = s.toArray();
			for (int i = 0; i < array.length; i++) {
				if (!this.contains(array[i])) {
					return false;
				}
			}
			return true;
		}

		public static SimpleSet createSet(Object... o) {
			SimpleSet s = new SimpleSet();
			for (int i = 0; i < o.length; i++) {
				s.add(o[i]);
			}
			count = count++;
			System.out.println("count" + count);
			return s;
		}

		public SimpleSet and(SimpleSet o) {
			SimpleSet ret = new SimpleSet();
			Object[] array = this.toArray();
			for (int i = 0; i < array.length; i++) {
				if (o.contains(array[i])) {
					ret.add(array[i]);
				}
			}
			return ret;
		}

		private void addAll(SimpleSet set) {
			Object[] array = set.toArray();
			for (int i = 0; i < array.length; i++) {
				this.add(array[i]);
			}
		}

		public SimpleSet or(SimpleSet set) {
			SimpleSet ret = new SimpleSet();
			ret.addAll(this);
			ret.addAll(set);
			return ret;
		}
	}