import java.util.*;

class MergeSort{
    static List<Integer> sort(List<Integer> old){
        int l=0;
        int r=old.size()-1;
        if(l<r){
            int m=l+(r-l)/2;
            List<Integer> s1=sort(old.subList(l, m+1));
            List<Integer> s2=sort(old.subList(m+1, r+1));
            return merge(s1, s2);
        }else{
            return old;
        }
    }

    static List<Integer> merge(List<Integer> L, List<Integer> R){//generic
        List<Integer> ret=new ArrayList<Integer>();
        int i=0;
        int j=0;
        while(i<L.size() && j<R.size()){
            int n1=L.get(i);
            int n2=R.get(j);
            if(n1<n2){
                ret.add(n1);
                i++;
            }else{
                ret.add(n2);
                j++;
            }
        }
        while(i<L.size()){
            int n=L.get(i);
            ret.add(n);
            i++;
        }
        while(j<R.size()){
            int n=R.get(j);
            ret.add(n);
            j++;
        }
        return ret;
    }
    public static void main(String [] args){
        List ret=sort(List.of(20, 25, 30, 100, 120, 15, 28, 45, 85, 90, 99));
        System.out.println(ret);
    }
}
