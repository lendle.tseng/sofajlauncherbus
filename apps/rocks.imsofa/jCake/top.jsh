//DEPS org.jgrapht:jgrapht-core:1.5.1
import org.jgrapht.*;
import org.jgrapht.graph.*;
import org.jgrapht.graph.builder.*;
import java.util.*;
import org.jgrapht.traverse.*;

Graph<String, DefaultWeightedEdge> g=GraphTypeBuilder.<String, DefaultWeightedEdge> directed().
    weighted(true).edgeClass(DefaultWeightedEdge.class).buildGraph();

for(int i=1; i<=6; i++){
    g.addVertex(""+i);
}

g.addEdge("1", "2");
g.addEdge("1", "3");
g.addEdge("2", "5");
g.addEdge("2", "4");
g.addEdge("3", "5");
g.addEdge("3", "4");
g.addEdge("4", "6");
g.addEdge("5", "6");

g.setEdgeWeight((DefaultWeightedEdge)g.getEdge("1", "2"), 2);
g.setEdgeWeight((DefaultWeightedEdge)g.getEdge("1", "3"), 5);
g.setEdgeWeight((DefaultWeightedEdge)g.getEdge("2", "5"), 10);
g.setEdgeWeight((DefaultWeightedEdge)g.getEdge("2", "4"), 5);
g.setEdgeWeight((DefaultWeightedEdge)g.getEdge("3", "5"), 8);
g.setEdgeWeight((DefaultWeightedEdge)g.getEdge("3", "4"), 9);
g.setEdgeWeight((DefaultWeightedEdge)g.getEdge("4", "6"), 4);
g.setEdgeWeight((DefaultWeightedEdge)g.getEdge("5", "6"), 3);

while(true){
    Set<String> vs=g.vertexSet();
    if(vs.isEmpty()){
        break;
    }
    String toBeDeleted=null;
    for(String v : vs){
        int inDegree=g.inDegreeOf(v);
        if(inDegree==0){
            toBeDeleted=v;
            break;
        }
    }
    System.out.println(toBeDeleted);
    g.removeVertex(toBeDeleted);
}

// TopologicalOrderIterator<String, DefaultWeightedEdge> it=new TopologicalOrderIterator<>(g);
// while(it.hasNext()){
//     String v=it.next();
//     System.out.println(v);
// }