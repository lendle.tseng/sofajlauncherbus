//DEPS org.jgrapht:jgrapht-core:1.5.1
import org.jgrapht.*;
import org.jgrapht.graph.*;
import org.jgrapht.graph.builder.*;
import java.util.*;


class SortableEdge implements Comparable<SortableEdge>{
	private String source = null, target = null;
	private double weight = 1;
	public SortableEdge(String source, String target, double weight){
		this.source=source;
		this.target=target;
		this.weight=weight;
	}

    public String getSource() {
		return source;
	}

	public String getTarget() {
		return target;
	}

	public double getWeight() {
		return weight;
	}

    public int compareTo(SortableEdge e){
        if(this.weight==e.weight){
            return 0;
        }else if(this.weight<e.weight){
            return -1;
        }else{
            return 1;
        }
    }
}

Graph<String, DefaultWeightedEdge> g = GraphTypeBuilder.<String, DefaultWeightedEdge>undirected().weighted(true).edgeClass(DefaultWeightedEdge.class).buildGraph();

List<SortableEdge> edges=new ArrayList<>();
edges.add(new SortableEdge("a","b",20));
edges.add(new SortableEdge("a","c",9));
edges.add(new SortableEdge("a","d",13));
edges.add(new SortableEdge("c","b",1));
edges.add(new SortableEdge("c","d",2));
edges.add(new SortableEdge("b","e",4));
edges.add(new SortableEdge("d","e",3));
edges.add(new SortableEdge("f","b",5));
edges.add(new SortableEdge("f","d",14));

Collections.sort(edges);
g.addVertex("a");
g.addVertex("b");
g.addVertex("c");
g.addVertex("d");
g.addVertex("e");
g.addVertex("f");

Set<String> set=new HashSet<>();

while(g.edgeSet().size()<5){
    SortableEdge candidate=edges.remove(0);
    if(set.contains(candidate.getSource()) && set.contains(candidate.getTarget())){
        continue;
    }else{
        g.addEdge(candidate.getSource(), candidate.getTarget());
        DefaultWeightedEdge thisEdge=g.getEdge(candidate.getSource(), candidate.getTarget());
        g.setEdgeWeight(thisEdge, candidate.getWeight());
        set.add(candidate.getSource());
        set.add(candidate.getTarget());
    }
}

System.out.println(g);

