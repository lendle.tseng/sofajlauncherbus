class Main{
    public static long hanoiTower(int n){
        if(n==1){
            return 1;
        }else{
            return hanoiTower(n-1)+1+hanoiTower(n-1);
        }
    }

    public static void main(String [] args) throws Exception{
        System.out.println(hanoiTower(30));
    }
}