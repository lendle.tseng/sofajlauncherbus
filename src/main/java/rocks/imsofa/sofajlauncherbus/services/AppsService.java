package rocks.imsofa.sofajlauncherbus.services;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rocks.imsofa.sofajlauncherbus.AppDatabase;
import rocks.imsofa.sofajlauncherbus.AppEntry;
import rocks.imsofa.sofajlauncherbus.ConfigurationManager;
import rocks.imsofa.sofajlauncherbus.utils.*;
import rocks.imsofa.sofajlauncherbus.utils.tasks.InstallTask;
import rocks.imsofa.sofajlauncherbus.utils.tasks.TaskInfoUtil;
import rocks.imsofa.sofajlauncherbus.utils.tasks.UninstallTask;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AppsService {
    @Autowired
    private AppDatabase appDatabase=null;

    @Autowired
    private ConfigurationManager conf=null;

    @Autowired
    private RunProcessUtil runProcessUtil=null;

    @Autowired
    private ThreadUtil threadUtil=null;

    @Autowired
    private TaskInfoUtil taskInfoUtil=null;

    private List<Integer> installedJdkVersions=null;

    public List<AppEntry> getAppEntries(AppsFilter appsFilter) throws Exception {
        List<AppEntry> appEntries = appDatabase.getAppEntries();
        List<AppEntry> ret = appEntries.stream()
                .filter(appsFilter).collect(Collectors.toList());
        return new ArrayList<>(ret);
    }

    public List<AppEntry> getAppEntries() throws Exception {
        return this.getAppEntries(AppsFilter.ALL);
    }

    public void install(AppEntry appEntry) throws Exception{
        threadUtil.submitSingleton(()->{
            try {
                InstallTask task=new InstallTask(appEntry);
                taskInfoUtil.addTask(task);
                File appsRoot=conf.getAppsRoot();
                //download it at first
                AppDownloader.download(appsRoot, appEntry);
                //run post install script
                JbangWrapper jbangWrapper=new JbangWrapper();
                jbangWrapper.runPostInstallScript(appEntry);
                //update appstatus
                appDatabase.reloadLocalAppEntries();
                task.markComplete();
                taskInfoUtil.removeTask(task.getId());
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }, false);

    }

    public void uninstall(AppEntry appEntry) throws Exception{
        if(appEntry.isInstalled()){
            threadUtil.submitSingleton(()->{
                try {
                    UninstallTask task=new UninstallTask(appEntry);
                    taskInfoUtil.addTask(task);
                    File appFolder = conf.getAppFolder(appEntry);
                    if (appFolder.exists()) {
                        //run pre uninstall script
                        JbangWrapper jbangWrapper = new JbangWrapper();
                        jbangWrapper.runPreUninstallScript(appEntry);
                        FileUtils.deleteDirectory(appFolder);
                        //update appstatus
                        appDatabase.reloadLocalAppEntries();
                    }
                    task.markComplete();
                    taskInfoUtil.removeTask(task.getId());
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }, false);

        }
    }

    /**
     * upgrade: preupgrade -> uninstall -> install -> postupgrade
     * @param appEntry
     * @throws Exception
     */
    public synchronized void upgrade(AppEntry appEntry) throws Exception{
        threadUtil.submitSingleton(()->{
            try {
                //run pre upgrade script
                JbangWrapper jbangWrapper=new JbangWrapper();
                jbangWrapper.runPreUpgradeScript(appEntry);

            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }, false);
        uninstall(appEntry);
        install(appEntry);
        threadUtil.submitSingleton(()->{
            try {
                //run post upgrade script
                JbangWrapper jbangWrapper=new JbangWrapper();
                jbangWrapper.runPostUpgradeScript(appEntry);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        },  false);
    }

    public synchronized boolean checkJdkAvailable(AppEntry appEntry) throws IOException {
        if(installedJdkVersions==null){
            installedJdkVersions=new JbangWrapper().getInstalledJdk();
        }
        String jdkVersionString=appEntry.getJdkVersion();
        if(jdkVersionString==null){
            jdkVersionString="8";
        }
        jdkVersionString=jdkVersionString.trim();
        if(jdkVersionString.endsWith("+")){
            jdkVersionString=jdkVersionString.substring(0, jdkVersionString.length()-1);
        }
        Integer jdkVersion=Integer.valueOf(jdkVersionString);
        for(Integer installedJdkVersion : installedJdkVersions){
            if(installedJdkVersion.doubleValue()>=jdkVersion.doubleValue()){
                return true;
            }
        }
        installedJdkVersions=null;
        return false;
    }

    public RunTaskSubmissionResult run(AppEntry appEntry) throws Exception{
        if(!checkJdkAvailable(appEntry)){
            return new RunTaskSubmissionResult(true, RunTaskSubmissionResult.JDK_VERSION_NOT_FOUND);
        }
        return runProcessUtil.run(appEntry);
    }
}
