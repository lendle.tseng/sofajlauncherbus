package rocks.imsofa.sofajlauncherbus.services;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.Gson;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rocks.imsofa.sjl.*;
import rocks.imsofa.sofajlauncherbus.utils.JbangWrapper;
import rocks.imsofa.sofajlauncherbus.utils.ThreadUtil;
import rocks.imsofa.sofajlauncherbus.utils.message.DiscardingResponseCallback;
import rocks.imsofa.sofajlauncherbus.utils.message.MessageClientWrapper;
import rocks.imsofa.sofajlauncherbus.utils.message.ResponseCallback;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

@Service
public class SystemService {
    private Map<ActionCommand, Integer> actionCommandToPortMap = new HashMap<>();
    @Autowired
    private ThreadUtil threadUtil;
    public boolean installJdk(double version){
        JbangWrapper jbangWrapper=new JbangWrapper();
        try {
            jbangWrapper.installJdk(version);
            List<Integer> installedJdkVersions=jbangWrapper.getInstalledJdk();
            if(installedJdkVersions.contains((int)version)){
                return true;
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return false;
    }

    public boolean updateAppDatabase() throws IOException {
        File tempFile= File.createTempFile("temp", ".json");
        boolean ret=HttpRequest.get("https://imsofa.rocks/apps/SofaJLauncherBus/apps.json").receive(tempFile).ok();
        if(ret){
            FileUtils.copyFile(tempFile, new File("apps.json"));
            return true;
        }
        return false;
    }

    public synchronized void registerActionCommands(ActionCommandRegistrationRequest request){
        for(ActionCommand actionCommand:request.getActionCommands()){
            System.out.println("registering "+actionCommand.getPackageName()+"."+actionCommand.getActionName()+":"+request.getPort());
            actionCommandToPortMap.put(actionCommand, request.getPort());
        }
    }

    public void executeActionCommand(ActionRequest actionRequest){
        Integer port=actionCommandToPortMap.get(actionRequest.getActionCommand());
        if(port==null){
            throw new RuntimeException("no port registered for action command "+actionRequest.getActionCommand());
        }
        threadUtil.submit(()->{
            Gson gson=new Gson();
            RemotePacket remotePacket=new RemotePacket(actionRequest);
            String json=gson.toJson(remotePacket);

            MessageClientWrapper.send("localhost", port, json, new ResponseCallback() {
                @Override
                public void onResponse(String response) {
                    //forward the request to the app on the source port
                    Logger.getLogger(this.getClass().getName()).info("send action response to "+actionRequest.getSourcePort());
                    MessageClientWrapper.send("localhost",
                            actionRequest.getSourcePort(),
                            response,
                            new DiscardingResponseCallback()
                    );
                }

                @Override
                public void onError(Throwable e) {
                    //return the error to the source port
                    RemotePacket remotePacket=new RemotePacket(new ActionResponse(actionRequest, null, true));
                    MessageClientWrapper.send("localhost",
                            actionRequest.getSourcePort(),
                            gson.toJson(remotePacket),
                            new DiscardingResponseCallback()
                    );
                }
            });
        });
    }
}
