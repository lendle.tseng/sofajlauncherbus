/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rocks.imsofa.sofajlauncherbus;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;

/**
 *
 * @author lendle
 */
@Component
public class ConfigurationManager {
    public File getAppsRoot(){
        File root=new File(".");
        File appsRoot=new File(root, "apps");
        return appsRoot;
    }
    
    public File getAppsJsonFile(){
        File root=new File(".");
        File appJsonFile=new File(root, "apps.json");
        return appJsonFile;
    }

    public AppEntry findAppEntry(String packageName, String name) throws IOException {
        File appJsonFile=this.getAppsJsonFile();
        String appJsonString= FileUtils.readFileToString(appJsonFile, "utf-8");
        Gson gson=new Gson();
        Type token = new TypeToken<List<AppEntry>>(){}.getType();
        List<AppEntry> appEntries=gson.fromJson(appJsonString, token);
        for(AppEntry entry : appEntries) {
            if (entry.getPackageName().equals(packageName) && entry.getName().equals(name)) {
                return entry;
            }
        }
        return null;
    }
    
    public File getAppFolder(AppEntry appEntry){
        File packageFolder=new File(getAppsRoot(), appEntry.getPackageName());
        File appFolder=new File(packageFolder, appEntry.getName());
        return appFolder;
    }
}
