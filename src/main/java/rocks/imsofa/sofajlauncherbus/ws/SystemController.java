package rocks.imsofa.sofajlauncherbus.ws;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import rocks.imsofa.sjl.ActionCommandRegistrationRequest;
import rocks.imsofa.sjl.ActionRequest;
import rocks.imsofa.sofajlauncherbus.services.SystemService;

import java.io.IOException;

@RestController
public class SystemController {
    @Autowired
    private SystemService systemService=null;

    @GetMapping("/installJdk/version/{version}")
    public boolean installJdk(@PathVariable double version){
        return systemService.installJdk(version);
    }

    @GetMapping("/updateAppDatabase")
    public boolean updateAppDatabase() throws IOException {
        return systemService.updateAppDatabase();
    }

    @PostMapping("/registerActionCommands")
    public void registerActionCommands(@RequestBody ActionCommandRegistrationRequest request){
        systemService.registerActionCommands(request);
    }

    @PostMapping("/executeActionCommand")
    public void executeActionCommand(@RequestBody  ActionRequest request){
        systemService.executeActionCommand(request);
    }
}
