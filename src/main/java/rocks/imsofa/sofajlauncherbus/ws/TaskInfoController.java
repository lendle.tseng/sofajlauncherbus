/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rocks.imsofa.sofajlauncherbus.ws;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import rocks.imsofa.sofajlauncherbus.utils.tasks.RunTask;
import rocks.imsofa.sofajlauncherbus.utils.tasks.Task;
import rocks.imsofa.sofajlauncherbus.utils.tasks.TaskInfoUtil;

/**
 *
 * @author lendle
 */
@RestController
public class TaskInfoController {
    @Autowired
    private TaskInfoUtil taskInfoUtil=null;
    
    @GetMapping("/taskInfo/id/{id}")
    public Task getTaskInfo(@PathVariable String id){
        return taskInfoUtil.getTaskInfo(id);
    }
    
    @GetMapping("/runTaskInfo")
    public List<RunTask> getRunTaskInfo(){
        return taskInfoUtil.getRunTaskInfo();
    }
    
    @GetMapping("/runTaskInfo/packageName/{packageName}/name/{name}")
    public List<RunTask> getRunTaskInfo(@PathVariable String packageName, @PathVariable String name){
        return taskInfoUtil.getRunTaskInfo(packageName, name);
    }
}
