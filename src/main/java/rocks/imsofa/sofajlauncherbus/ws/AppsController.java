/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rocks.imsofa.sofajlauncherbus.ws;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import rocks.imsofa.sofajlauncherbus.AppDatabase;
import rocks.imsofa.sofajlauncherbus.AppEntry;
import rocks.imsofa.sofajlauncherbus.services.AppsService;
import rocks.imsofa.sofajlauncherbus.utils.AppsFilter;
import rocks.imsofa.sofajlauncherbus.utils.RunTaskSubmissionResult;

import java.util.List;

/**
 *
 * @author lendle
 */
@RestController
public class AppsController {
    @Autowired
    private AppsService appsService;

    @Autowired
    private AppDatabase appDatabase=null;

    @GetMapping("/apps/installed")
    public List<AppEntry> getInstalledApps() throws Exception{
        return appsService.getAppEntries(AppsFilter.INSTALLED);
    }
    
    @GetMapping("/apps/installable")
    private List<AppEntry> getInstallableApps() throws Exception {
        return appsService.getAppEntries(AppsFilter.INSTALLABLE);
    }

    @PostMapping("/install")
    public void install(@RequestBody AppEntry appEntry) throws Exception{
        appsService.install(appEntry);
    }

    @PutMapping("/upgrade")
    public void upgrade(@RequestBody AppEntry appEntry) throws Exception{
        appsService.upgrade(appEntry);
    }

    @DeleteMapping("/uninstall/packageName/{packageName}/name/{name}")
    public void uninstall(@PathVariable  String packageName, @PathVariable  String name) throws Exception{
        AppEntry appEntry=appDatabase.getAppEntry(packageName, name);
        appsService.uninstall(appEntry);
    }

    @PostMapping("/run")
    public RunTaskSubmissionResult run(@RequestBody AppEntry appEntry) throws Exception {
        return appsService.run(appEntry);
    }
}
