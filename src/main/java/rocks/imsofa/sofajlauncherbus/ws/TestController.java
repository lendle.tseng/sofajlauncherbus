package rocks.imsofa.sofajlauncherbus.ws;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import rocks.imsofa.sofajlauncherbus.utils.JbangWrapper;

import java.io.IOException;
import java.util.List;

@RestController
public class TestController {
    @GetMapping("/test")
    public List<Integer> test(){
        JbangWrapper jbangWrapper=new JbangWrapper();
        try {
            List<Integer> jdks=jbangWrapper.getInstalledJdk();
            return jdks;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
