/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rocks.imsofa.sofajlauncherbus.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 *
 * @author USER
 */
@Controller
public class ShowAppsController {
    @GetMapping("/showInstallable")
    public String showInstallable(){
        return "showInstallable";
    }
    
    @GetMapping("/showInstalled")
    public String showInstalled(){
        return "showInstalled";
    }
}
