/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rocks.imsofa.sofajlauncherbus.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 *
 * @author lendle
 */
@Controller
public class ShowProcessesController {
    @GetMapping("/showProcesses/packageName/{packageName}/name/{name}")
    public String indexAction(@PathVariable String packageName, @PathVariable String name, Model model){
        model.addAttribute("packageName", packageName);
        model.addAttribute("name", name);
        return "showProcesses";
    }
}
