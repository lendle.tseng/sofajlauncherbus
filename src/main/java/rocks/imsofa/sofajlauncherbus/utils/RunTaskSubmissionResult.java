package rocks.imsofa.sofajlauncherbus.utils;

public class RunTaskSubmissionResult {
    public static final int SUCCESS=0, JDK_VERSION_NOT_FOUND=1, UNKNOWN_ERROR=2;
    private boolean error=false;
    private int statusCode=SUCCESS;
    private String runTaskId=null;

    public RunTaskSubmissionResult(boolean error, String runTaskId, int statusCode) {
        this.error = error;
        this.statusCode = statusCode;
        this.runTaskId=runTaskId;
    }

    public RunTaskSubmissionResult(boolean error, int statusCode) {
        this.error = error;
        this.statusCode = statusCode;
    }

    public RunTaskSubmissionResult() {
    }

    public String getRunTaskId() {
        return runTaskId;
    }

    public void setRunTaskId(String runTaskId) {
        this.runTaskId = runTaskId;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }
}
