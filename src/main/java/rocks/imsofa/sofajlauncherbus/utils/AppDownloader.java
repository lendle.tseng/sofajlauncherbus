package rocks.imsofa.sofajlauncherbus.utils;

import com.google.gson.Gson;
import net.lingala.zip4j.core.ZipFile;
import org.apache.commons.io.FileUtils;
import rocks.imsofa.sofajlauncherbus.AppEntry;

import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.util.Map;

public class AppDownloader {
    public static void download(File appsRoot, AppEntry appEntry) throws Exception {
        Gson gson=new Gson();
        //download it
        URL url=new URL(appEntry.getUrl());
        try(InputStream input=url.openStream()){
            File tempFolder=new File(System.getProperty("java.io.tmpdir"));
            File tempFile=new File(tempFolder, url.getFile());
            FileUtils.copyInputStreamToFile(input, tempFile);
            ZipFile zipFile=new ZipFile(tempFile);
            File packageFolder=new File(appsRoot, appEntry.getPackageName());
            if(!packageFolder.exists()){
                packageFolder.mkdirs();
            }
            File appFolder=new File(packageFolder, appEntry.getName());
            if(!appFolder.exists()){
                appFolder.mkdirs();
            }

            zipFile.extractAll(appFolder.getAbsolutePath());
            File [] unzippedFiles=appFolder.listFiles();
            if(unzippedFiles.length==1 && unzippedFiles[0].isDirectory()){
                File nestedRootFolder=unzippedFiles[0];
                //then move everything to its parent folder
                for(File file : unzippedFiles[0].listFiles()){
                    if(file.getName().equals(nestedRootFolder.getName()) && file.isDirectory()){
                        continue;
                    }else if(file.isFile()){
                        FileUtils.moveFileToDirectory(file, appFolder, true);
                    }else if(file.isDirectory()){
                        FileUtils.moveDirectoryToDirectory(file, appFolder, true);
                    }
                }
            }

            //save app meta data into .app-inf folder
            File appMetadataFolder=new File(appFolder, ".app-inf");
            appMetadataFolder.mkdirs();
            File appMetadataFile=new File(appMetadataFolder, "info.json");
            FileUtils.write(appMetadataFile, gson.toJson(Map.of("version", appEntry.getVersion())), "utf-8");
            //remove temp files
            FileUtils.delete(tempFile);
        }
    }
}
