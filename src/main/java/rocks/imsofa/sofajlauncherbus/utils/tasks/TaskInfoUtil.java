/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rocks.imsofa.sofajlauncherbus.utils.tasks;

import org.springframework.stereotype.Component;
import rocks.imsofa.sofajlauncherbus.AppEntry;
import rocks.imsofa.sofajlauncherbus.utils.ProcessWrapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author lendle
 */
@Component
public class TaskInfoUtil {
    private Map<String, Task> taskInfos=new HashMap<>();
    private static final long MAX_INTERVAL_AFTER_COMPLETED=60000;
    private static final long MAX_INTERVAL_NO_PROGRESS=600000;
    
    public TaskInfoUtil(){
        //launch a background daemon to remove old tasks
        Thread t=new Thread(()->{
            while(true){
                try {
                    Thread.sleep(60000);
                    List<String> toBeRemoved=new ArrayList<>();
                    long time=System.currentTimeMillis();
                    for(Task info : taskInfos.values()){
                        if(info instanceof RunTask){
                            RunTask runTask= (RunTask) info;
                            if(!runTask.isDone() && !runTask.getProcessWrapper().getNativeProcess().isAlive()){
                                runTask.markComplete();
                            }
                        }
                        if(info.getProgress()==0){
                            if((time-info.getStartTime())>MAX_INTERVAL_NO_PROGRESS){
                                //this task has no progress in the amount of time
                                toBeRemoved.add(info.getId());
                            }
                        }else if(info.getProgress()==1){
                            System.out.println((time-info.getCompletedTime())+":"+MAX_INTERVAL_AFTER_COMPLETED);
                            if((time-info.getCompletedTime())>MAX_INTERVAL_AFTER_COMPLETED){
                                //this task has been completed and kept for a long amount of time
                                toBeRemoved.add(info.getId());
                            }
                        }
                    }
                    for(String id : toBeRemoved){
                        taskInfos.remove(id);
                    }
                } catch (InterruptedException ex) {
                    Logger.getLogger(TaskInfoUtil.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        t.setDaemon(true);
        t.start();
    }

    public synchronized void addTask(Task task){
        this.taskInfos.put(task.getId(), task);
    }

    public synchronized void removeTask(String id){
        this.taskInfos.remove(id);
    }

    public RunTask createRunTaskInfo(ProcessWrapper processWrapper){
        RunTask info=new RunTask(processWrapper);
        info.setStartTime(System.currentTimeMillis());
        info.setId(""+System.currentTimeMillis());
        addTask(info);
        return info;
    }
    
    public List<RunTask> getRunTaskInfo(String packageName, String name){
        List<RunTask> ret=new ArrayList<>();
        for(Task task : taskInfos.values()){
            if(task instanceof RunTask){
                RunTask runTaskInfo=(RunTask) task;
                AppEntry appEntry=runTaskInfo.getProcessWrapper().getAppEntry();
                if(appEntry.getPackageName().equals(packageName) && appEntry.getName().equals(name)){
                    ret.add(runTaskInfo);
                }
            }
        }
        return ret;
    }
    
    public List<RunTask> getRunTaskInfo(){
        List<RunTask> ret=new ArrayList<>();
        for(Task task : taskInfos.values()){
            if(task instanceof RunTask){
                RunTask runTaskInfo=(RunTask) task;
                ret.add(runTaskInfo);
            }
        }
        return ret;
    }
    
    public Task getTaskInfo(String id){
        return taskInfos.get(id);
    }
}
