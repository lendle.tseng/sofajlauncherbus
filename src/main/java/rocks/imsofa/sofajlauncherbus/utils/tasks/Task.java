/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package rocks.imsofa.sofajlauncherbus.utils.tasks;

/**
 *
 * @author lendle
 */
public interface Task {
    public String getId();
    public double getProgress();
    public boolean isDone();
    public boolean isIndeterminate();
    public long getStartTime();
    public long getCompletedTime();
    public boolean requestTerminate();
}
