package rocks.imsofa.sofajlauncherbus.utils.tasks;

import rocks.imsofa.sofajlauncherbus.AppEntry;

public class UninstallTask extends AbstractTask{
    private AppEntry appEntry=null;

    public UninstallTask(AppEntry appEntry) {
        this.appEntry = appEntry;
    }

    public AppEntry getAppEntry() {
        return appEntry;
    }

    @Override
    public boolean requestTerminate() {
        return false;
    }
}
