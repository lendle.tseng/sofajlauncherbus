/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rocks.imsofa.sofajlauncherbus.utils.tasks;

import lombok.Data;
import rocks.imsofa.sofajlauncherbus.utils.ProcessWrapper;

/**
 *
 * @author lendle
 */
@Data
public class RunTask extends AbstractTask {
    private int exitCode=-1;
    private ProcessWrapper processWrapper=null;
    
    public RunTask(ProcessWrapper processWrapper) {
        this.processWrapper=processWrapper;
    }

    public int getExitCode() {
        if(!processWrapper.getNativeProcess().isAlive()){
            return processWrapper.getNativeProcess().exitValue();
        }else{
            return -1;
        }
    }

    @Override
    public boolean requestTerminate() {
        processWrapper.getNativeProcess().destroy();
        return true;
    }

    public ProcessWrapper getProcessWrapper() {
        return processWrapper;
    }
}
