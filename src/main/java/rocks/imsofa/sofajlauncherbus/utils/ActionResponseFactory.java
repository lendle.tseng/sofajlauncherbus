package rocks.imsofa.sofajlauncherbus.utils;

import rocks.imsofa.sjl.ActionCommand;
import rocks.imsofa.sjl.ActionRequest;
import rocks.imsofa.sjl.ActionResponse;

public class ActionResponseFactory {
    public static ActionResponse createNotSupportedActionResponse(ActionRequest request){
        return new ActionResponse(request, "{'message':'not supported'}", true);
    }
}
