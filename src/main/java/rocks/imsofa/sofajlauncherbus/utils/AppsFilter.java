package rocks.imsofa.sofajlauncherbus.utils;

import rocks.imsofa.sofajlauncherbus.AppEntry;

import java.util.function.Predicate;

public interface AppsFilter extends Predicate<AppEntry> {
    public boolean test(AppEntry appEntry);

    public static final AppsFilter INSTALLED=new AppsFilter() {
        @Override
        public boolean test(AppEntry appEntry) {
            return appEntry.isInstalled();
        }
    };

    public static final AppsFilter INSTALLABLE=new AppsFilter() {
        @Override
        public boolean test(AppEntry appEntry) {
            return !appEntry.isInstalled() || appEntry.isUpgradable();
        }
    };

    public static final AppsFilter ALL=new AppsFilter() {
        @Override
        public boolean test(AppEntry appEntry) {
            return true;
        }
    };
}
