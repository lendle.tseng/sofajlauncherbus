package rocks.imsofa.sofajlauncherbus.utils;

import rocks.imsofa.sofajlauncherbus.AppEntry;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class JbangWrapper {
    private File jbangBinaryRoot=null;

    public JbangWrapper(File jbangBinaryRoot) {
        this.jbangBinaryRoot = jbangBinaryRoot;
    }

    public JbangWrapper() {
        File jbangDir = new File("jbang");
        File binDir = new File(jbangDir, "bin");
        this.jbangBinaryRoot=binDir;
    }
    
    private boolean isScriptExists(AppEntry appEntry, String scriptName){
        File appFolder = getAppFolder(appEntry);
        File scriptFile=new File(appFolder, scriptName);
        return scriptFile.exists() && scriptFile.isFile();
    }

    /**
     *
     * @param homeDirectory the home directory to run jbang
     * @param jbangArgs args without the jbang command itself
     * @return
     */
    private ProcessBuilder createProcessBuilder(File homeDirectory, List<String> jbangArgs){
        File jbangExecutable = null;

        String os = System.getProperty("os.name").toLowerCase();
        if (os.contains("win")) {
            jbangExecutable = new File(jbangBinaryRoot, "jbang.cmd");
        } else {
            jbangExecutable = new File(jbangBinaryRoot, "jbang");
        }
        List<String> args=new ArrayList<>();
        args.add(jbangExecutable.getAbsolutePath());
        args.addAll(jbangArgs);
        ProcessBuilder pb = new ProcessBuilder(args);
        pb.directory(homeDirectory);
        pb.redirectErrorStream(true);
        return pb;
    }

    private ProcessBuilder createProcessBuilder(
            String jbangCommand, Map<String, String> commandAargs, File homeDirectory, String scriptName) {
        List<String> args=new ArrayList<>();
        args.add(jbangCommand);
        for(String key : commandAargs.keySet()){
            args.add(key+"="+commandAargs.get(key));
        }
        args.add(scriptName);
        return this.createProcessBuilder(homeDirectory, args);
    }

    private ProcessBuilder createProcessBuilder(AppEntry appEntry, String scriptName) {
        File appFolder = getAppFolder(appEntry);

        File libsFolder = new File(appFolder, "libs");
        List<String> jarFileList = new ArrayList<>();
        for (File jarFile : libsFolder.listFiles()) {
            if (jarFile.getName().endsWith(".jar")) {
                jarFileList.add("libs" + File.separator + jarFile.getName());
            }
        }
        String cp = String.join(File.pathSeparator, jarFileList);
        return this.createProcessBuilder("run", Map.of(
                "--java", appEntry.getJdkVersion(),
                "--cp", cp
        ), appFolder, scriptName);

    }

    private static File getAppFolder(AppEntry appEntry) {
        File appsFolder = new File("apps");
        File packageFolder = new File(appsFolder, appEntry.getPackageName());
        File appFolder = new File(packageFolder, appEntry.getName());
        return appFolder;
    }

    public ProcessWrapper runLauncherScript(AppEntry appEntry) throws IOException {
        if(!isScriptExists(appEntry, "launcher.java")){
            throw new IOException("launcher.java not exists");
        }
        ProcessBuilder pb=this.createProcessBuilder(appEntry, "launcher.java");
        Process process = pb.start();
        ProcessWrapper processWrapper=new ProcessWrapper(process, appEntry);


        return processWrapper;
    }

    public void runPostInstallScript(AppEntry appEntry) throws IOException {
        if(isScriptExists(appEntry, "install.java")){
            ProcessBuilder pb=this.createProcessBuilder(appEntry, "install.java");
            pb.start();
        }
    }

    public void runPreUninstallScript(AppEntry appEntry) throws IOException {
        if(isScriptExists(appEntry, "uninstall.java")){
            ProcessBuilder pb=this.createProcessBuilder(appEntry, "uninstall.java");
            pb.start();
        }
    }

    /**
     * execute the pre upgrade script, which usually contains the code to backup the app's data
     * @param appEntry
     * @throws IOException
     */
    public void runPreUpgradeScript(AppEntry appEntry) throws IOException {
        if(isScriptExists(appEntry, "preupgrade.java")){
            ProcessBuilder pb=this.createProcessBuilder(appEntry, "preupgrade.java");
            pb.start();
        }
    }

    /**
     * execute the post upgrade script, which usually contains the code to restore the app's data
     * @param appEntry
     * @throws IOException
     */
    public void runPostUpgradeScript(AppEntry appEntry) throws IOException {
        if(isScriptExists(appEntry, "postupgrade.java")){
            ProcessBuilder pb=this.createProcessBuilder(appEntry, "postupgrade.java");
            pb.start();
        }
    }

    public List<Integer> getInstalledJdk() throws IOException {
        ProcessBuilder pb=this.createProcessBuilder(new File("."),
                List.of("jdk", "list"));
        Process process = pb.start();
        BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
        List<Integer> ret=new ArrayList<>();
        while (true) {
            String line = reader.readLine();
            if (line != null) {
                line=line.trim();
                if(Character.isDigit(line.charAt(0))){
                    String jdkVersion=line.split(" ")[0].trim();
                    ret.add(Integer.valueOf(jdkVersion));
                }
            } else {
                break;
            }
        }
        return ret;
    }

    public void installJdk(double version) throws Exception {
        ProcessBuilder pb=this.createProcessBuilder(new File("."),
                List.of("jdk", "install", ""+(int)version));
        Process process = pb.start();
        BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
        while (true) {
            String line = reader.readLine();
            if (line != null) {
                line=line.trim();
                System.out.println(line);
            } else {
                break;
            }
        }
        int ret=process.waitFor();
        if(ret!=0){
            throw new Exception("installation failed");
        }
    }
}
