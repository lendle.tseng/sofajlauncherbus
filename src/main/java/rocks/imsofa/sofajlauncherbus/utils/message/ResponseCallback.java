package rocks.imsofa.sofajlauncherbus.utils.message;

public interface ResponseCallback {
    public void onResponse(String response);

    public void onError(Throwable e);
}
