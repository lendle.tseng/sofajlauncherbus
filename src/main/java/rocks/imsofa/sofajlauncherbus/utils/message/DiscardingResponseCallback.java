package rocks.imsofa.sofajlauncherbus.utils.message;

import java.util.logging.Logger;

public class DiscardingResponseCallback implements ResponseCallback{
    @Override
    public void onResponse(String response) {

    }

    @Override
    public void onError(Throwable e) {
        Logger.getLogger(this.getClass().getName()).severe(e.getMessage());
    }
}
