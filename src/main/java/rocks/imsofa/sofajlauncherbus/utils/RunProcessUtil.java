/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rocks.imsofa.sofajlauncherbus.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import rocks.imsofa.sofajlauncherbus.AppEntry;
import rocks.imsofa.sofajlauncherbus.utils.tasks.RunTask;
import rocks.imsofa.sofajlauncherbus.utils.tasks.TaskInfoUtil;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author lendle
 */
@Component
public class RunProcessUtil {

    @Autowired
    private ThreadUtil threadUtil = null;

    @Autowired
    private TaskInfoUtil taskFactoryUtil=null;

    public RunTaskSubmissionResult run(final AppEntry appEntry) {
        try {
            JbangWrapper jbangWrapper=new JbangWrapper();
            ProcessWrapper processWrapper=jbangWrapper.runLauncherScript(appEntry);
            RunTask runTaskInfo=taskFactoryUtil.createRunTaskInfo(processWrapper);
            threadUtil.submit(() -> {
                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(processWrapper.getNativeProcess().getInputStream()));

                    while (true) {
                        String line = reader.readLine();
                        if (line != null) {
                            processWrapper.getOutput().append("\r\n").append(line);
                        } else {
                            break;
                        }
                    }

                    runTaskInfo.setDone(true);
                    runTaskInfo.setProgress(1);
                } catch (IOException ex) {
                    Logger.getLogger(RunProcessUtil.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
            return new RunTaskSubmissionResult(false, runTaskInfo.getId(), RunTaskSubmissionResult.SUCCESS);
        } catch (IOException e) {
            return new RunTaskSubmissionResult(true, RunTaskSubmissionResult.UNKNOWN_ERROR);
        }
    }
}
