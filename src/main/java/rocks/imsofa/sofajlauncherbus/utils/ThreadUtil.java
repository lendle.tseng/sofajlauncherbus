/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rocks.imsofa.sofajlauncherbus.utils;

import org.springframework.stereotype.Component;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 *
 * @author lendle
 */
@Component
public class ThreadUtil {
    private final ExecutorService executorService=Executors.newCachedThreadPool();
    private final ExecutorService singleThreadExecutorService=Executors.newFixedThreadPool(1);
    
    public void submit(Runnable r){
        executorService.submit(r);
    }
    
    public void shutdown(){
        executorService.shutdownNow();
        singleThreadExecutorService.shutdownNow();
    }

    public void submitSingleton(Runnable runnable, boolean async){
        if(async){
            singleThreadExecutorService.submit(runnable);
        }else{
            runnable.run();
        }
    }

    public void submitSingleton(Runnable runnable){
        submitSingleton(runnable, true);
    }
}
