package rocks.imsofa.sofajlauncherbus.utils;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import rocks.imsofa.sofajlauncherbus.AppEntry;
@JsonIgnoreProperties(value = { "nativeProcess" })
public class ProcessWrapper {
    private Process nativeProcess=null;
    private StringBuilder output=new StringBuilder();
    private AppEntry appEntry=null;

    public StringBuilder getOutput() {
        return output;
    }

    public ProcessWrapper(Process nativeProcess, AppEntry appEntry) {
        this.nativeProcess = nativeProcess;
        this.appEntry=appEntry;
    }

    public Process getNativeProcess() {
        return nativeProcess;
    }

    public AppEntry getAppEntry() {
        return appEntry;
    }
}
