package rocks.imsofa.sofajlauncherbus;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class AppDatabase {
    private List<AppEntry> appEntries=null;
    @Autowired
    private ConfigurationManager conf=null;

    public synchronized void reloadLocalAppEntries() throws IOException {
        File appJsonFile=conf.getAppsJsonFile();
        String appJsonString= FileUtils.readFileToString(appJsonFile, "utf-8");
//        System.out.println(appJsonString);
        Gson gson=new Gson();
        Type token = new TypeToken<List<AppEntry>>(){}.getType();
        this.appEntries=gson.fromJson(appJsonString, token);
        //refresh status of installed and upgradable
        for(AppEntry entry : appEntries){
            File appFolder=conf.getAppFolder(entry);
            File appMetadataFolder=new File(appFolder, ".app-inf");
            File appMetadataFile=new File(appMetadataFolder, "info.json");
            boolean installed=false;
            boolean upgradable=false;
            if(appMetadataFile.exists() && appMetadataFile.isFile()){
                installed=true;
                Map appMetadata=gson.fromJson(FileUtils.readFileToString(appMetadataFile, "utf-8"), Map.class);
                if(!entry.getVersion().equals(appMetadata.get("version"))){
                    upgradable=true;
                }
            }
            entry.setInstalled(installed);
            entry.setUpgradable(upgradable);
        }
    }

    public List<AppEntry> getAppEntries() throws IOException {
        if(this.appEntries==null){
            this.reloadLocalAppEntries();
        }
        return new ArrayList<>(this.appEntries);
    }

    public AppEntry getAppEntry(String packageName, String name) throws Exception {
        for(AppEntry appEntry : getAppEntries()){
            if(appEntry.getPackageName().equals(packageName) && appEntry.getName().equals(name)){
                return appEntry;
            }
        }
        throw new Exception("app not found");
    }
}
