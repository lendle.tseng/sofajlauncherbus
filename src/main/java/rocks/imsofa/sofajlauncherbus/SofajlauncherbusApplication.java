package rocks.imsofa.sofajlauncherbus;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import rocks.imsofa.sofajlauncherbus.utils.ThreadUtil;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URI;

@SpringBootApplication
public class SofajlauncherbusApplication {
    private static ConfigurableApplicationContext context;
    private static boolean springbootExitCalled=false;
    private TrayIcon trayIcon;
    @Autowired
    private ThreadUtil threadUtil=null;

    public static void main(String[] args) {
//        SpringApplication.run(SofajlauncherbusApplication.class, args);
        SpringApplicationBuilder builder = new SpringApplicationBuilder(SofajlauncherbusApplication.class);
        builder.headless(false);
        context = builder.run(args);
    }
    @PostConstruct
    public void init() {
        trayIcon = null;
        if (SystemTray.isSupported()) {
            // get the SystemTray instance
            SystemTray tray = SystemTray.getSystemTray();
            // load an image
            Image image = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/icon.jpeg"));

            // create a popup menu
            PopupMenu popup = new PopupMenu();
            // create menu item for the default action
            MenuItem openGUIMenuItem = new MenuItem("Open GUI");
            openGUIMenuItem.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    if (Desktop.isDesktopSupported()) {
                        Desktop desktop = Desktop.getDesktop();
                        if (desktop.isSupported(Desktop.Action.BROWSE)) {
                            try {
                                desktop.browse(URI.create("http://localhost:8221"));
                            } catch (IOException ex) {
                                throw new RuntimeException(ex);
                            }
                        }
                    }
                }
            });
            popup.add(openGUIMenuItem);

            MenuItem exitMenuItem = new MenuItem("Shutdown");
            exitMenuItem.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    springbootExitCalled=true;
                    SpringApplication.exit(context, () -> 0);
                }
            });
            popup.add(exitMenuItem);
            // construct a TrayIcon
            trayIcon = new TrayIcon(image, "SJL", popup);
            trayIcon.setImageAutoSize(true);
            // set the TrayIcon properties
//            trayIcon.addActionListener(listener);
            // ...
            // add the tray image
            try {
                tray.add(trayIcon);
//                System.out.println("Tray image added");
            } catch (AWTException e) {
                System.err.println(e);
            }
        } else {
            System.out.println("System tray is not supported");
        }
    }
    @PreDestroy
    public void destroy() {
        if(trayIcon!=null){
            SystemTray.getSystemTray().remove(trayIcon);
        }
        if(!springbootExitCalled){
            SpringApplication.exit(context, () -> 0);
        }else{
//            System.out.println("springboot exit called");
            threadUtil.shutdown();
            System.exit(0);
        }
    }
}
