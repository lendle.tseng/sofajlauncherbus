# Installation

SofaJLauncherBus is distributed as a binary zip file.
To install SofaJLauncherBus, unzip the binary zip file to a directory of your choice.
The root directory of the binary zip file is the directory that contains the following files:

    root/
    |-apps/
    |-jre/
    |-jbang/
    |-sofajlauncherbus-0.0.1-SNAPSHOT.jar
    |-config.json
    |-JbangJdkLauncher-1.0-SNAPSHOT-jar-with-dependencies.jar
    |-run.sh
    |-run.bat
    |-run.java
    |-sjl.java
    |-apps.json

To launch SofaJLauncherBus, run the run.sh or run.bat script based on your operating system.

## Linux

To launch SofaJLauncherBus on Linux, run the run.sh script.

## Windows

To launch SofaJLauncherBus on Windows, run the run.bat script.

---

Once SofaJLauncherBus is launched, you can see an icon shown in the system tray:

![SofaJLauncherBus icon](images/icon.jpeg)