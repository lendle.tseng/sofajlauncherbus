//DEPS com.github.kevinsawicki:http-request:5.3
//DEPS com.google.code.gson:gson:2.10.1
//DEPS net.sourceforge.argparse4j:argparse4j:0.9.0
//DEPS de.vandermeer:asciitable:0.3.2
import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.*;
import net.sourceforge.argparse4j.*;
import net.sourceforge.argparse4j.inf.*;
import java.util.*;
import de.vandermeer.asciitable.*;
import net.sourceforge.argparse4j.impl.*;
public class sjl{
	private static Map getAppEntry(String fullName, boolean installed) throws Exception{
		Gson gson=new Gson();
		String mode=(installed)?"installed":"installable";
		String str=HttpRequest.get("http://localhost:8221/apps/"+mode).body();
		List<Map> apps=gson.fromJson(str, List.class);
		for(Map app : apps){
			if(fullName.equals(""+app.get("packageName")+"."+app.get("name"))){
				return app;
			}
		}
		return null;
	}
	public static void main(String [] args) throws Exception{
		ArgumentParser parser = ArgumentParsers.newFor("sjl").build()
                .defaultHelp(true)
                .description("Cli tool to SofaJavaLauncher");
		
		Subparsers subparsers = parser.addSubparsers().help("Sub Commands").dest("command");
		Subparser parserList = subparsers.addParser("list").help("list apps");
		parserList.addArgument("--installable")
			.dest("mode")
			.action(Arguments.storeConst())
			.setConst("installable")
			.help("Show Installable Apps");
		parserList.addArgument("--installed")
			.dest("mode")
			.action(Arguments.storeConst())
			.setConst("installed")
			.help("Show Installed Apps");
		
		Subparser parserUpdate = subparsers.addParser("update").help("update app database");
		
		Subparser parserRun = subparsers.addParser("run").help("run app");
		
		parserRun.addArgument("fullName")
			.type(String.class)
			.help("the fully qualified name of the app");
			
		Subparser parserInstall = subparsers.addParser("install").help("install app");
		
		parserInstall.addArgument("fullName")
			.type(String.class)
			.help("the fully qualified name of the app");
		
		Subparser parserRemove = subparsers.addParser("remove").help("uninstall app");
		
		parserRemove.addArgument("fullName")
			.type(String.class)
			.help("the fully qualified name of the app");
		
		Namespace ns = null;
        try {
            ns = parser.parseArgs(args);
			String command=ns.getString("command");
			if("list".equals(command)){
				String mode=ns.getString("mode");
				if(mode==null){
					mode="installed";
				}
				Gson gson=new Gson();
				String str=HttpRequest.get("http://localhost:8221/apps/"+mode).body();
				List<Map> apps=gson.fromJson(str, List.class);
				AsciiTable at = new AsciiTable();
				at.addRule();
				at.addRow("Package", "Name", "Version", "Description");
				for(Map app : apps){
					at.addRule();
					at.addRow(app.get("packageName"), app.get("name"), app.get("version"), app.get("description"));
				}
				at.addRule();
				System.out.println(at.render());
			}else if("update".equals(command)){
				String str=HttpRequest.get("http://localhost:8221/updateAppDatabase").body();
				if("true".equals(str)){
					System.out.println("app database updated");
				}else{
					System.out.println("app database update failed");
				}
			}else if("run".equals(command)){
				String fullName=ns.getString("fullName");
				//locate app entry
				Gson gson=new Gson();
				Map app=getAppEntry(fullName, true);
				if(app!=null){
					String resultStr=HttpRequest.post("http://localhost:8221/run").contentType("application/json").send(gson.toJson(app)).body();
					Map result=gson.fromJson(resultStr, Map.class);
					if(Boolean.valueOf(""+result.get("error"))){
						if(1==Double.valueOf(""+result.get("statusCode"))){
							//install jdk at first
							System.out.println("installing required jdk "+app.get("jdkVersion")+"...");
							HttpRequest.get("http://localhost:8221/installJdk/version/"+app.get("jdkVersion")).body();
							resultStr=HttpRequest.post("http://localhost:8221/run").contentType("application/json").send(gson.toJson(app)).body();
							result=gson.fromJson(resultStr, Map.class);
							if(Boolean.valueOf(""+result.get("error"))){
								System.out.println("fail to submit task");
							}else{
								System.out.println("task submitted");
							}
						}
						System.out.println("task submitted");
					}else{
						System.out.println("task submitted");
					}
				}else{
					System.out.println("app not found");
				}
				
			}else if("install".equals(command)){
				String fullName=ns.getString("fullName");
				Gson gson=new Gson();
				Map app=getAppEntry(fullName, false);
				if(app!=null){
					HttpRequest.post("http://localhost:8221/install").contentType("application/json").send(gson.toJson(app)).body();
				}
			}else if("remove".equals(command)){
				String fullName=ns.getString("fullName");
				Gson gson=new Gson();
				Map app=getAppEntry(fullName, true);
				if(app!=null){
					int index=fullName.lastIndexOf(".");
					String packageName=fullName.substring(0, index);
					String name=fullName.substring(index+1);
					String str=HttpRequest.delete("http://localhost:8221/uninstall/packageName/"+packageName+"/name/"+name).body();
					System.out.println(str);
				}
			}
			
        } catch (ArgumentParserException e) {
            parser.handleError(e);
            System.exit(1);
        }
	}
}
